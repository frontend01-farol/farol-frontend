$(document).ready(function(){
	$('.sidebar__submenu').click(function(){
		toggleSubmenu($(this).attr("href"));
	});
});

$(document).click(function(e){
	if($(e.target).is(".sidebar__container") && !$(e.target).is(".sidebar")){
		toggleMenu();
	}
});

function toggleSubmenu(idSubmenu){
	idSubmenu = 'a[href="'+idSubmenu+'"]';
	var elemento = document.querySelectorAll(idSubmenu);

	$(elemento).toggleClass('submenu--item-active');
}

function toggleMenu(){
  $('.navbar').toggleClass('btn-sidebar-active');
  $('body').toggleClass('overflow');
}
